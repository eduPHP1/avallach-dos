# Dev Assessment: Avallac'h

## Overview

This is a brand new **Laravel** (version `^7.24`) install for **PHP** version `^7.4`. The goal here is to create a 
little "look-up app" where delimited files are the data sources (to be imported).

You can accomplish the objectives in various ways:

- A fully functional web application that can run locally via **Valet**, `$ php -S localhost:8000`, etc.
- With a suite of tests that showcase how the application is able to perform all objectives
- Pseudo-code with comments that allow the reviewer to read through and envision how the application would work
- Update this `README.md` with your thoughts, approach, intentions and/or explanations of your logic

Now granted, a functional application with some high level integration tests would be optimal but time restrictions and/or
experience will vary and we encourage you to showcase your problem solving skills in as many ways possible.

> There are **a lot** of ways to leverage TDD / Testing, the Laravel framework, and popular Composer packages... showcase that here!

### Time (~30 to 60 minutes)

We respect your time and for this assessment, we would encourage you to take approximately 30-60 minutes or so writing 
and committing code. You are free to spend as long as you'd like - but if you start getting pressed for time, feel free 
to finish up by writing out tests you would *eventually* get passing or write out some pseudo-code with helpful comments.

## Discovery & Clarification

Currently, we have the Issue board *open* if you would like to ask some questions for clarification - we understand that 
this README may not completely cover everything you need to get started. You can create a new Issue here:
https://gitlab.com/s90dev/avallach-dos

## Objectives

We have some source data that lives within two delimited data files. We need to somehow load these into our local 
database and give the "user" or API consumer a way to look up applicable data based on them providing the `DATA_ID` 
via a query payload described below.

The `data-parent` file are fictional novels and the `data-child` file contains characters within those books.

### Setup

- Import the files' data into the database (MySQL)
  - `storage/sources/data-parent.csv`
  - `storage/sources/data-child.txt`
- *Something to consider:* What if we are given new, updated files in the future before release?
- *Important Component:* The process has applicable tests
- As you develop, feel free to bring in any necessary Composer packages and be ready to discuss *why*

### Functionality

- A Laravel `artisan` command to initiate the import process for the delimited source files
- It would be great to track the above commands in some sort of database log to see the history of imports
- A `POST` request can be sent to a route requesting a "look-up"
- Some sort of validation on the request based on the data type/structure of `DATA_ID`
- The look-up is logged in the database so we have a history of look-up requests
- The request payload (`GET`) **requires** a `search_query` value (see below) - if missing respond accordingly. That value will 
correspond to the `data-parent.csv` source file's `DATA_ID`
- If the payload is missing the above value the application simply responds with an applicable `4**` HTTP status
- *Bonus:* A user can submit the `DATA_ID` value *without* the dashes and still get the same result
- *Bonus:* Query results are cached appropriately for quick re-look-up(s); Cache busters are ready as well
- *Important Component:* The processes have applicable tests

### Notes

- The settings for **PHPUnit** (`phpunit.xml`) have already been set to use an in-memory database (`sqlite`)
- The base `TestCase` is leveraging the `RefreshDatabase` trait

### Application Features

#### Request & Reponse(s)

- Upon a successful `200 OK` request, our application's *MVP release* simply responds with a `JSON` response payload. 
- The response payload structure should be as follows:

```
{
    "data": {
        "isbn": "value-here",
        "title": "Title Here",
        "year": ####,
        "characters": [
            {
                "full_name": "Name Here",
                "created_at": "Y-m-d H:i:s e"
            }
        ]
    }
}
```

- So, for example, the following submission:

```
POST /path/to/your/endpoint
Content-Type: application/json

{
    "search_query": "978-0-316-27371-8"
}
```

... would result in the following `JSON` response:

```
{
    "data": {
        "isbn": "978-0-316-27371-8",
        "title": "The Tower of the Swallow",
        "year": 1997,
        "characters": [
            {
                "full_name": "Cirilla Fiona Elen Riannon",
                "created_at": "1991-02-20 04:30:00 UTC"
            },
            {
                "full_name": "Dandylion",
                "created_at": "1992-07-20 12:30:00 UTC"
            },
            {
                "full_name": "Geralt of Rivia",
                "created_at": "1989-04-01 04:30:00 UTC"
            }
        ]
    }
}
```

- *Bonus:* If you build out a simple UI (not required), it knows how to read / display the response `JSON` in the browser 
(if you do build a UI, a basic **Vue** (`^2.6.11`) app is ready to go within the `welcome.blade.php` file)
- *Important Component:* The processes have applicable tests

## Fork, Commit & Push!

Feel free to commit as often as you normally would when working with `git` and version control. When you are complete, 
please push the changes either to your own forked **Gitlab** remote or as a **Merge Request** to 
`https://gitlab.com/s90dev/avallach-dos`

If you are scheduled for a **pair programming** session as part of your technical interview, we can also review your branch 
during your scheduled session by sharing your screen (see below).

### Pair Programming Session / Technical Interview Preparation

Please install **VS Code** with the **Live Share** (Microsoft) plugin to share your work. If you have another, preferred 
IDE sharing method, please contact S90 and let us know first.

We will most likely integrate the **TALL** stack into the project. We will start by:

- Installing with basic configuration
- Leveraging the `Livewire` PHPUnit extension in order to provide the ability to:
  - Edit a **Novel** / Add a new **Novel**
  - Add **Character(s)** to a **Novel**
  - Edit existing **Character(s)**
  - Table / List views for **Novel** / **Character** data

Enjoy.
